import { Facture } from './facture';

export class Client {

    id : number;	
	nomClient : string;
	telephone : string;
	email : string;
    adresse : string;
    factures : Facture[]

}