import { Client } from './client';


export class Facture {

    id : number;

	client : Client;
	
	reference : string;
	
	dateEcheance : string;
	
	montantTotal : number;

}