import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { Observable } from 'rxjs';
import { Client } from '../shared/model/client';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-client',
  templateUrl: './updclient.component.html',
  styleUrls: ['./client.component.css']
})
export class UpdClientComponent implements OnInit {

  id: number;
  updClient : Observable<Client>;

  constructor(private clientService: ClientService, private router: Router, private route: ActivatedRoute) {
    
   }

  ngOnInit() {

      console.log("Chargement d'un client");

      //this.listClients = this.clientService.getClients();

      this.id = this.route.snapshot.params['id'];
      this.clientService.getClient(this.id)
        .subscribe(data => { console.log( data ); this.updClient = data});

      console.log(this.updClient);
  }

  updateClient() {
    console.log("Updating");
     this.clientService.updateClient(this.id, this.updClient)
      .subscribe(data => console.log(data), error => console.log(error));
  }

  OnSubmit() {
    console.log("Submitting");
    this.updateClient();
  }

}
