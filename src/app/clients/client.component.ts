import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { Observable } from 'rxjs';
import { Client } from '../shared/model/client';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  message: string;
  listClients : Observable<Client[]>;

  constructor(private clientService: ClientService, private router: Router) {
    
   }

  ngOnInit() {

      console.log("Chargement de la liste des clients");

      this.listClients = this.clientService.getClients();

      console.log(this.listClients);
  }

  updateClient(id: number) {
    this.router.navigate(['updclient',id]);  
  }

}
