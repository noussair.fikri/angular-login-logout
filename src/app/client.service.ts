import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from './shared/model/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'http://localhost:9000/api/client';

  getClients(): Observable<any> {
        return this.http.get(this.baseUrl+'/clients');
  }

  getClient(id:number): Observable<any> {
    return this.http.get(this.baseUrl+'/client/'+id);
  }

  createClient(client: Object): Observable<Object> {
    return this.http.post(this.baseUrl+'/ajout', client)
  }

  updateClient(id:number, client: Object): Observable<Object> {
    console.log("Servicing");
    return this.http.put(this.baseUrl+'/modif/'+id, client)
  }

  deleteClient(id:number): Observable<Object> {
    return this.http.delete(this.baseUrl+'/supprime/'+id);
  }

}
